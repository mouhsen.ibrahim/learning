start:
	minikube start --kubernetes-version=1.20.14

context:
	kubectl config use-context minikube
