# Setup
Create a python venv by using this command

```
python3 -m venv venv
```

Activate the new environment with this command

```
source venv/bin/activate
```

Install needed python packages with this command

```
pip install -r requirements.txt
```

Execute the basics main.py with these commands

```
cd Basics
python main.py test.yaml
```

If you ommit `test.yaml` it will read the file called `data.yaml` and parse it.


# YAML references
You can define a reference in YAML using `&` followed by the refernece name and then its value like this

```yaml
- &SS Sammy Sosa
```

You can use this reference later like this

```yaml
- *SS
```

This helps you to re-use values in YAML

# Multiline strings
You can use `|` to create a multi line string where each line is separated from the next with `\n`

```yaml
- |
    hello
    there
```

This will procuce this text

```
hello\nthere\n
```

While using `>` will not create new lines

```yaml
- >
    hello
    there
```

This will produce this output

```
hello there\n
```

# Tags
?????
