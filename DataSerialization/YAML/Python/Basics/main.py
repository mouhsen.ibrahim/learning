
import sys

import parse

def main():
    try:
        data_file = sys.argv[1]
    except IndexError:
        data_file = "data.yaml"
    print("process file", data_file)
    parse.parse_file(data_file)
    data = """
    - Mark McGwire
    - Sammy Sosa
    - Ken Griffey
    """
    d = parse.parse_str(data)
    print(d)
    data = """
    hr:  65    # Home runs
    avg: 0.278 # Batting average
    rbi: 147   # Runs Batted In
    """
    d = parse.parse_str(data)
    print(d)
    data = """
    - [name        , hr, avg  ]
    - [Mark McGwire, 65, 0.278]
    - [Sammy Sosa  , 63, 0.288]
    """
    d = parse.parse_str(data)
    print(d)

if __name__ == "__main__":
    main()
