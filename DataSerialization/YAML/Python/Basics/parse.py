import yaml

def parse_file(file_name):
    with open(file_name, "r") as f:
        data = yaml.safe_load_all(f)
        for d in data:
            print(d)

def parse_str(s):
    return yaml.safe_load(s)
