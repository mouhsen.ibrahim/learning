# Introduction

[Keptn](https://keptn.sh) is a cloud native application life-cycle orchestration tool, it help us to manage
deployments and versions for our services from development to production, it also integrates well with
other tools such as ArgoCD, jenkins, FluxCS, Github Actions, etc...

It helps us to define multi stage delivery pipelines and monitor our SLIs/SLOs for any violation, it also
automatilaly scales and fixes some issues.

## Why Keptn?

When working in a micro service enviornment, we need to manage deployments to multiple environments
with multiple versions, we need to ensure that when we deploy to production we ran tests on staging
first and that our new deployment does not break our SLOs and works as expected, also we need
to be able to rollback quickly in case of errors and get alerts if deployments fail.

Keptn helps us to achieve these goals with a cloud native way, it also does not lock us in
for any deployment or testing tool, it integrates well with other tools and CI/CD solutions.

## Use cases

* I would like to see which version of which microservice is deployed to which environment and get a link for its manifests and source code.
* I need to ensure that before deploying to production, my services are running well in staging and tests are passing.
* I need to check my SLIs/SLOs and error budget and respect them when deploying.
* I need to get alerts if any deployments fail at any time.

With Keptn you can do all of these and more ....
