# learning

This repository holds my own learning tasks

Each directory represents a topic and each su-directory represents a tool used to achieve the tasks
in the topic, to learn more about the topic check the README file in the topic's folder, and to learn
more about every tool check the README in the tool's folder.

# Directory Structure
Top level directories are the topic names, for example: ServiceMesh, CI, CD, Monitoring, IaC, etc...

Inside each topic directory we find directories for tools used in each topic such as Istio, Terraform,
Prometheous, ArgoCD, Ansible, etc...

Inside each tool directory we find multiple tasks for every tool, these tasks are specific for the tool used
and there are Makefiles and READMEs for every tools and task which explain the task and contain targets
to deploy the code used in the task.

Every makefile must define some common targets such as build, test, publish and deploy for tasks, download, install
and delete for tools, so we can prepare any task quickly for testing and a showcase.

Some tasks depend on other tasks to be deployed to work.

To deploy a task cd into its directory and execute `make deploy`

# Topics and Tools

Here is a list of topics and the tools used in them with brief descriptions

## Service Meshes
A service mesh is essential in the microservices world, it helps to manage complex routing and monitoring
in the microservices, for more information check this [link](https://www.redhat.com/en/topics/microservices/what-is-a-service-mesh)

### Istio
Istio is an open source service mesh, it is used to control the workflow between services in a microservice architecture, more information
about istio can be found in this [link](https://www.redhat.com/en/topics/microservices/what-is-a-service-mesh) and in istio's website [here](https://istio.io)

## Data Serialization
Here we discuss the tools used to serialize data into a structure that can be exchanged

### YAML
YAML is a human-friendly data serialization language for all programming languages, according to its website [here](https://yaml.org)
