# Contribution Guide

In this guide I will try to explain my own way for learning new technologies and tools in my work.
I hope it will help me and others to learn better in a vast and quickly changing world.

## Introduction

Learning new tools and technologies is a hard task to do, it requires concentration, motivation to learn
and setting up an end goal for learning which is not a simple thing to do, here are some steps that might help:

* Answer the why? Find and write the reason why you are learning something, do not say I want to learn
    X because it is important or has a good future, rather say I want to learn X because it will help me
    to achieve Y and Z. e.g. I want to learn rust programming language because it will help me to write
    better software in terms of memory safety and ensuring high performance while writing in a high level
    programming language that offers me protection against many errors and at the same time ensures performance.

* Set up a goal: It is the most important step, a goal helps you to measure your progress towards it, and
    with a goal you awlays know what you will achieve at the end which motivates you.

* Create a schedule: Organize your learning process using a schedule, you can use a milestone in Gitlab and
    plan your work using issues that are added to the milestone.

## Steps

Here I will write about the concret steps that you need to follow to learn a new technology or tool.

### Write the reason to learn the new tool

Create a directory for the new tool/technology in its own topic directory and add a `README.md`, inside
this file write about the tool, where it is used and how it can enhance your development work from
development to production. Make it short and concise about your tool and target.

Think about a use case for the tool and show how it can serve you in this case with a comparison how it is
like without the tool, think as a user and ensure that you can justify using this tool, this section could
be modified later when you get better experience with the tool so you can give better justifications to your choices.

### Write your goals

Your goals should be written in a created milestone for the learning process, they should be measurable so you
can measure how much did you achieve and how much is remaining, write them in a check list that you can tick
once done, your created issues will be taken primarly from these goals.

### Create issues for your process

Now after the milestone is created and your goals are set, you need to write issues about what you must do, start
with simple getting started guides, identify the important actions using this tool and create issues from them
to dig deeper in them, ensure to have use cases with your issues and state how they help you to reach your goals.

## Conclusion

Enjoy and document your learning process, also try to write articles about how to use what you learned in real
use cases, DO NOT write articles about how to use the tool but about how to achieve something useful using it,
this will attract more readers and help you to justify using this tool when it is offered to your boss or team.
